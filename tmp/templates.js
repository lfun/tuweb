(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['pic'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "";
  buffer += "\n	<div class=\"item\">\n		<img src=\""
    + escapeExpression((typeof depth0 === functionType ? depth0.apply(depth0) : depth0))
    + "\" alt=\"\" class=\"img-rounded\">\n	</div>\n";
  return buffer;
  }

  stack1 = helpers.each.call(depth0, depth0.picUrl, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n";
  return buffer;
  });
templates['rows'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n		<li class=\"span3\">\n			<div class=\"thumbnail\">\n				<img data-src=\"holder.js/300x200\" alt=\"\" src=\""
    + escapeExpression(((stack1 = depth0.picUrl),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" class=\"img-rounded\" id=\""
    + escapeExpression(((stack1 = depth0.id),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\">\n				<p>"
    + escapeExpression(((stack1 = depth0.title),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</p>\n				<h3>"
    + escapeExpression(((stack1 = depth0.category),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</h3>\n			</div>\n		</li>\n		";
  return buffer;
  }

  buffer += "<div class=\"row-fluid\" id=\"firstrow\">\n	<ul class=\"thumbnails\">\n		";
  stack1 = helpers.each.call(depth0, depth0.storys, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "    	\n	</ul>\n</div>";
  return buffer;
  });
templates['story'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "			<div class=\"thumbnail\">\n				<img data-src=\"holder.js/300x200\" alt=\"\" src=\""
    + escapeExpression(((stack1 = depth0.picUrl),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" class=\"img-rounded\" id=\""
    + escapeExpression(((stack1 = depth0.id),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\">\n				<p>"
    + escapeExpression(((stack1 = depth0.title),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</p>\n				<h3>"
    + escapeExpression(((stack1 = depth0.category),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</h3>\n			</div>\n";
  return buffer;
  });
})();