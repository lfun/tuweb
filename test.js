var Document=Backbone.Model.extend({
	title:'title',
	content:'content'
}) ;
var DocumentView=Backbone.View.extend({
	render:function(){
		this.$el.append('<h1>'+this.model.get('title')+'</h1>');
		this.$el.append('<div>'+this.model.get('content')+'</div>');
		return this;
	}
});
var Documents=Backbone.Collection.extend({
	model:Document
});



var DocumentTitleListView=Backbone.View.extend(	{
	tagName:'ul',	
	model:documents,
	render:function(){

		this.model.forEach(function(model){
			this.$el.append(
				new DocumentTitleView({model:model}).render().el);
		},this);
	
		return this;
	}
})
var eventAggregator=_.extend({},Backbone.Events);
var DocumentTitleView=Backbone.View.extend({
	tagName:'h1',
	model:Document,
	events:{
		'click':function(){
			//need show document 
			//need change the path
			eventAggregator.trigger('document:selected',this.model);
		}
	},
	render:function(){
		this.$el.append(this.model.get('title'));
		return this;
	}
})


var document1=new Document();
document1.set('title','title1');
document1.set('content','this is content 1');

var document2=new Document();
document2.set('title','title2');
document2.set('content','this is content 2');

var documents=new Documents();
documents.add(document1);
documents.add(document2);



var DocumentRouter=Backbone.Router.extend({
	routes:{
		'contents':'contents',
		'view/:title':'viewDocument'
	},
	contents:function(){

		$('body').html(new DocumentTitleListView({model:documents}).render().el);
	},
	viewDocument:function(title){
		var selectedDocument=documents.findWhere({"title":title});
		$('body').empty().html(new DocumentView({model:selectedDocument}).render().el);
	}
})

eventAggregator.on('document:selected',function(document){
	var urlPath='view/'+document.get('title');
	router.navigate(urlPath,{trigger:true});
})

var router=new DocumentRouter();
Backbone.history.start();
router.navigate('contents',{trigger:true});